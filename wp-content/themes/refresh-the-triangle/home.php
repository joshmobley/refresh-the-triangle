<?php get_header(); ?>

<div class="page-section about">

	<p>Refresh the Triangle is a meetup organized around bringing together developers, designers, content creators, project managers, and anyone
		else who works inside the digital space to discuss and share big ideas about how to improve at our craft, delight our stakeholders, and 
		create quality experiences online.</p>

</div>

<div class="page-section next-meetup">

	<h2 class="page-section-title">Lightning Talks: Creativity</h2>

	<h3 class="date-and-time">October 8 @ 7:00pm</h3>

	<div class="meetup-description">

		<p>We're kicking off a new era in RTT's history with a session of&nbsp;5&mdash;minute&nbsp;lightning talks based on varying facets of Creativity.</p>
	
		<p>Presentations will include:</p>
	
		<ul>
			<li>Encouraging Divergent Thinking</li>
			<li>Fostering Creativity in an Uninspiring Environment</li>
			<li>Building a Space for Creative Work</li>
			<li>...and more!</li>
		</ul>

	</div>

	<div class="meetup-details">
	
		<dl>
			<dt>Date</dt>
			<dd>October 8, 2015</dd>
			<dt>Time</dt>
			<dd>7:00 pm</dd>
			<dt>Venue</dt>
			<dd>HQ Raleigh</dd>
			<dt>Address</dt>
			<dd>108 Martin St. Raleigh, NC 27606</dd>
			<dt>RSVPs</dt>
			<dd>38 Going</dd>
		</dl>
	
		<a class="button" href="#">RSVP on Meetup.com</a>

	</div>

</div>

<!--<div class="other-meetups">

	<h2>Upcoming Meetups</h2>


</div>-->

<div class="talk page-section">

	<h2 class="page-section-title">Have Something to Share?</h2>

	<p>If you'd like to participate in the upcoming lightning talks, or have a presentation you'd like to share in the future, let us know!</p>

	<form>

		<fieldset>
			<label for="name">Your Name</label>
			<input type="text" name="name" placeholder="First and last, please." />
			<label for="occupation">What You Do</label>
			<input type="text" name="occupation" placeholder="What do people call you at work?" />
			<label for="organization">Where You Do It</label>
			<input type="text" name="organization" placeholder="This one's optional." />
			<label for="talk-title">Title of Your Presentation</label>
			<input type="text" name="talk-title" placeholder="Doesn't have to be final." />
			<label for="talk-description">Tell Us a Little About It</label>
			<textarea name="talk-description" placeholder="A few sentences is fine."></textarea>
			<input type="submit" value="Submit" />
		</fieldset>

	</form>

</div>

<div class="page-section sponsorship clear-fix">

	<h2 class="page-section-title">Our Sponsors</h2>

	<p>We'd love for your organization to help us bring together some of the Triangle's brightest digital talent. If you have a space in Raleigh or Durham that you'd be willing to open to us a few times a year, we'd love to hear from you.</p>


	

</div>

<?php get_footer(); ?>